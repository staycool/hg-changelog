const path = require('path');
const Promise = require('bluebird');
const _ = require('lodash');
const moment = require('moment');
const hg = require('hg');

const HGRepo = hg.HGRepo;
Promise.promisifyAll(HGRepo.prototype);

const dir = path.resolve(process.cwd());
const name = process.cwd().split(path.sep).reverse(0)[0]
const repo = new HGRepo(dir);

const messageRegexp = /^(feat|chore|style|refactor):(?:\s)?(.*)$/;

async function getChangelog() {
    let rows = await repo.logAsync();
    rows = rows
        .map(x => x.body)
        .filter(x => x)
        .map(x => {
            return _.fromPairs(
                x.split('\n')
                    .filter(x => x)
                    .map(x => {
                        const r = /^([a-z]+):\s+(.+)$/
                        const matches = x.match(r);
                        return matches && [matches[1], matches[2]];
                    })
                    .filter(x => x)
            );
        })
        .filter(x => x.summary.match(messageRegexp));
    let changelog = `# ${name} backlog\n\n`;
    const byDate = _(rows)
        .map(x => ({
            user: x.user,
            date: moment(new Date(x.date)).startOf('day'),
            commit: x.changeset.split(':')[1],
            message: x.summary
        }))
        .groupBy(x => x.date)
        .map((x, y) => [y, x])
        .sortBy(x => new Date(x[0]))
        .reverse()
        .forEach(day => {
            const date = moment(new Date(day[0])).format('LL');
            const commits = day[1];
            changelog += `### ${date}\n`;
            commits.forEach(commit => {
                const entry = `+ ${commit.message} ([${commit.commit}](https://bitbucket.org/staycool/${name}/commits/${commit.commit}))`;
                changelog += entry + '\n';
            });
            changelog += '\n\n';
        });

    console.log(changelog);
}

getChangelog();
